function createList(array, parent = document.body) {
    const parr = document.body
    const ul = document.createElement('ul');
    console.log(parent);
    parent.append(ul);
    
    array.forEach(item => {
      const li = document.createElement('li');
      li.textContent = item;
      ul.appendChild(li);
    });
  }
  
  const array = ['Diana', 'Margo', 'Vika', '24', '28'];
  createList(array); 


// 1. Опишіть, як можна створити новий HTML тег на сторінці.
// Є два способи створення DOM вузлів:
// 1. document.createElement(tag) - cтворює новий елемент з заданим тегом.
// Наприклад: let div = document.createElement('div');
// 2. document.createTextNode(text) - cтворює новий текстовий вузол з заданим текстом.
// Наприклад: let textNode = document.createTextNode('От і я');


// 2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// insertAdjacentHTML()розбирає вказаний текст як HTML або XML і встановлює отримані вузли (вузли) у дереві DOM у вказаній позиції. Ця функція не переписує наявні елементи, що передбачає додаткову серіалізацію, і тому працює швидше, ніж маніпуляції з innerHTML. 
// Параметри бувають: position та text. 
// Position:
// DOMString- визначає позицію доданого елемента відносно елемента, викликаного методом. Потрібно відповідати одному з наступних значень:
// 'beforebegin': до самого element(до відкриваючого тега).
// 'afterbegin': сразу после открывающего тега element(перед первым потомком).
// 'beforeend': сразу перед закрывающим тегом element(после последнего потомка).
// 'afterend': после element(после закрывающего тега).
// Text:
// Строка, яка буде проаналізована як HTML або XML і вставлена ​​в DOM-дерево документа.



// 3. Як можна видалити елемент зі сторінки?
  // Метод Element.remove()видаляє елемент з DOM-дерева, в якому він знаходиться.